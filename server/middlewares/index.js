import expressJwt from 'express-jwt';

// everything is done as the package
// same secret that use to generate the token
// use the secret to verify the token
// error or allow the code to folow to the next stage
export const requireSignin = expressJwt({
  secret: process.env.JWT_SECRET,
  algorithms: ['HS256'],
});
