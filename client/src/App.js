//axios is configured in context
import './App.css';
//components
import Nav from './components/Nav';
//pages
import Account from './pages/Account';
import Basic from './pages/plans/Basic';
import Standard from './pages/plans/Standard';
import Premium from './pages/plans/Premium';
import Home from './pages/Home';
import Register from './pages/Register';
import Login from './pages/Login';
import NotFound from './pages/NotFound';
//global functions
import { Toaster } from 'react-hot-toast';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
// protected routes
import AuthRoute from './components/routes/AuthRoute';
import StripeCancel from './pages/stripe-cancel';
import StripeSuccess from './pages/stripe-success';

function App() {
  return (
    <Router>
      <Nav />
      <Toaster
        position="top-right"
        toastOptions={{
          duration: 2500,
        }}
      />
      <Routes>
        <Route element={<Home />} path="/" />
        <Route element={<Register />} path="/register" />
        <Route element={<Login />} path="/login" />
        {/* protected route */}

        {/* <Route element={<AuthRoute />}>
          <Route path="/stripe/success" element={<StripeSuccess />} />
        </Route> */}

        <Route
          element={
            <AuthRoute>
              <Basic />
            </AuthRoute>
          }
          path="/basic"
        />
        <Route
          element={
            <AuthRoute>
              <Standard />
            </AuthRoute>
          }
          path="/standard"
        />
        <Route
          element={
            <AuthRoute>
              <Premium />
            </AuthRoute>
          }
          path="/premium"
        />
        <Route
          element={
            <AuthRoute>
              <StripeSuccess />
            </AuthRoute>
          }
          path="/stripe/success"
        />
        <Route
          element={
            <AuthRoute>
              <StripeCancel />
            </AuthRoute>
          }
          path="/stripe/cancel"
        />
        <Route
          element={
            <AuthRoute>
              <Account />
            </AuthRoute>
          }
          path="/account"
        />
        {/* end protected route*/}
        <Route path="*" element={<NotFound />} />
      </Routes>
    </Router>
  );
}

export default App;
