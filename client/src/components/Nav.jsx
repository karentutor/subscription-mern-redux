import React, { Fragment, useContext } from 'react';
import { Link } from 'react-router-dom';
import { useNavigate } from 'react-router';
import { UserContext } from '../context';

const Nav = () => {
  const [state, setState] = useContext(UserContext);
  const navigate = useNavigate();

  const logout = () => {
    setState({ user: {}, token: '' });
    localStorage.removeItem('auth');
    navigate('/login');
  };

  // console.log('STATE => ', state);

  return (
    <ul className="nav border">
      <li className="nav-item">
        <Link className="nav-link" aria-current="page" to="/">
          Home
        </Link>
      </li>

      {state && state.token ? (
        <li className="nav-item dropdown">
          <Link
            className="nav-link dropdown-toggle"
            data-bs-toggle="dropdown"
            to="#"
            role="button"
            aria-expanded="false"
          >
            {state.user.email}
          </Link>
          <ul className="dropdown-menu">
            <li>
              <Link className="dropdown-item" to="/account">
                Account
              </Link>
            </li>
            <li>
              <hr className="dropdown-divider" />
            </li>
            <li>
              <span className="dropdown-item" onClick={logout}>
                Logout
              </span>
            </li>
          </ul>
        </li>
      ) : (
        <Fragment>
          <li className="nav-item">
            <Link className="nav-link" to="/register">
              Sign up
            </Link>
          </li>
          <li className="nav-item">
            <Link className="nav-link" to="/login">
              Login
            </Link>
          </li>
        </Fragment>
      )}
    </ul>
  );
};

export default Nav;
// <Fragment>
//   <li className="nav-item">
//     <span onClick={logout} className="nav-link">
//       Logout
//     </span>
//   </li>
// </Fragment>
