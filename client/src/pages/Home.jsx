import React, { useEffect, useState, useContext } from 'react';
import axios from 'axios';
import PriceCard from '../components/cards/PriceCard';
import { useNavigate } from 'react-router';
import { UserContext } from '../context'; // make sure that have access to state

// let cardTestData = [
//   {
//     id: 'price_1KBTNiLJG2N8cwg0yTiTg0vy',
//     price: '5000',
//     nickname: 'BASIC',
//     unit_amount: '5000',
//   },
//   {
//     id: 'price_1KBTRJLJG2N8cwg0ycZwdyHB',
//     price: '10000',
//     nickname: 'STANDARD',
//     unit_amount: '10000',
//   },
//   {
//     id: 'price_1KBTRdLJG2N8cwg0SMhINdRG',
//     price: '15000',
//     nickname: 'PREMIUM',
//     unit_amount: '15000',
//   },
// ];

const Home = () => {
  const [state, setState] = useContext(UserContext);
  const [prices, setPrices] = useState([]);
  const [userSubscriptions, setUserSubscriptions] = useState([]);

  const navigate = useNavigate();

  useEffect(() => {
    fetchPrices();
  }, []);

  useEffect(() => {
    let result = [];
    // note the cascading check to ensure no fail along the way
    // if this --> then.this ---> then.this.this etc
    const check = () =>
      state &&
      state.user &&
      state.user.subscriptions &&
      // populate an array of result
      state.user.subscriptions.map((sub) => {
        result.push(sub.plan.id);
      });
    check();
    setUserSubscriptions(result);
    //run when we have state and state.user
  }, [state && state.user]);

  const fetchPrices = async () => {
    const { data } = await axios.get('/prices');
    // console.log('prices get request', data);
    setPrices(data);
    // setPrices(cardTestData);
  };

  const handleClick = async (e, price) => {
    e.preventDefault();

    // if they  have the plan  -- take them to that plan page -- whatever it may be called
    // you will need to create the plan pages
    // do not let them continue in their efforts to purchase
    if (userSubscriptions && userSubscriptions.includes(price.id)) {
      navigate.push(`/${price.nickname.toLowerCase()}`);
      return;
    }

    // make sure that have state first
    if (state && state.token) {
      //stripe returns a new checkout window
      console.log('called');
      const { data } = await axios.post('/create-subscription', {
        priceId: price.id,
      });
      //stripe will us a one time page / url user can securely enter credit card payment
      // we open stripes own page
      window.open(data);
    } else {
      // no logged in user
      navigate('/register');
    }
  };

  return (
    <div className="container-fluid">
      <div className="row col-md-6 offset-md-3 text-center">
        <h1 className="pt-5 fw-bold">
          Explore the right plan for your business
        </h1>
        <p className="lead pb-4">Choose a plan that suites you best!</p>
      </div>

      {/* subscription are passed to the price card*/}
      <div className="row pt-5 mb-3 text-center">
        {prices &&
          prices.map((price) => (
            <PriceCard
              key={price.id}
              price={price}
              handleSubscription={handleClick}
              userSubscriptions={userSubscriptions}
            />
          ))}
      </div>
    </div>
  );
};

export default Home;
