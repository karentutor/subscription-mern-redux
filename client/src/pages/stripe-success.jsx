import React, { useContext, useEffect } from 'react';
import axios from 'axios';
import { SyncOutlined } from '@ant-design/icons';
import { useNavigate } from 'react-router';
import { UserContext } from '../context';

const StripeSuccess = () => {
  const navigate = useNavigate();
  const [state, setState] = useContext(UserContext);

  useEffect(() => {
    //    console.log('state', state);
    const getSubscriptionStatus = async () => {
      console.log('called get subscription');
      try {
        const { data } = await axios.get('/subscription-status');
        console.log('SUBSCRIPTION STATUS => ', data);
        // no subscription
        if (data && data.length === 0) {
          navigate('/');
        } else {
          // update user in local storage
          // has values token & user
          const auth = JSON.parse(localStorage.getItem('auth'));
          // here we just update the user infor with the subscription info in data
          auth.user = data; // -- not just user updated
          localStorage.setItem('auth', JSON.stringify(auth));
          // update user in context
          setState(auth);
          // here we simulate a promise -- ensure fully updated
          setTimeout(() => {
            navigate('/account');
          }, 1000);
        }
      } catch (err) {
        console.log('err');
      }
    };

    if (state && state.token) getSubscriptionStatus();
  }, [state && state.token]);

  return (
    <div
      className="d-flex justify-content-center fw-bold"
      style={{ height: '90vh' }}
    >
      <div className="d-flex align-items-center">
        <SyncOutlined spin style={{ fontSize: '50px' }} />
      </div>
    </div>
  );
};

export default StripeSuccess;
